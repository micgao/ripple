---
title: "Services"
keywords:
  - studio
  - recording
  - audio
  - mixing
mainservice: "Mixing"
mainservicedesc:
  [
    "Mixing service for any artist looking to get the most out of their performances. I aim to deliver balanced and dynamic mixes that leave an emotional impact on the listener, without compromising on the elements that will allow your tracks to compete on any modern streaming platforms and beyond.", 
  ]
secondaryservice: "Recording"
secondaryservicedesc:
  [
    "Straightforward digital recording session, whether it's for a song, EP, full album, voiceover work, or podcast. A list of available microphones and gear can be provided by request. Remote sessions can be also arranged for the group of almost eight billion people living nowhere near Ottawa, but find themselves wanting to make sure their demo is as well recorded as possible.",
  ]
otherservices: "Others"
otherservicesdesc:
  [
    "With how vast the range of audio related needs can be, I am open to projects that extend beyond just mixing and recording sessions; from cleaning up the soundtrack of a film or podcast to audio repair work like restoring damaged audio from an old VHS tape or CD.",
  ]
price: "Price"
pricedesc:
  [
    "There's nothing worse than shopping around online, even worse when it's in-person, and there's no mention of price anywhere. Or maybe that's just a poor person thing. Anyways, here's the breakdown, with currency indicated in canadian dollars:", "- The cost for a full mix will vary greatly, but will be 350$ or less. Average cost per song will be lower for EPs and full albums. An exact quote can only be provided once I have heard the demo", "- In-person recording sessions are 350$/full day. A 50$ surcharge will be applied for each significant other of band members at the session. Reach out for more information about remote sessions", "- Quote for any other service will be provided by request. Every effort will be made for my services to be affordable by all who really needs it, so feel free to reach out no matter your financial situation", "- 20% discount if paying in Monero",
  ]
---
