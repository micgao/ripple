---
title: "This Town"
keywords:
- pop
- music
- energetic
summary: "Angelo Boltini (unofficial remix)"

---

{{< musicplayer name="This Town" artist="Angelo Boltini (unofficial remix)" artwork="This Town" >}}

Special thanks to the artist for sharing the multitracks to this wonderful song and allowing it to be used for purposes
such as this one.

Please go support the artist directly by streaming the official release
on [Spotify](https://open.spotify.com/album/3Gif96GX4Ae5icVTWK4SeD), and considering purchasing the record
on [Bandcamp](https://angeloboltini.bandcamp.com/releases) if you like it!
