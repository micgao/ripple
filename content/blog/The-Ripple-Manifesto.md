---
title: Ripple Manifesto
keywords:
- manifesto
- studio
image: ""
feature_image: ""
---

---
##### The value of purpose
---

As stated at the bottom of this very page, though no harm in repeating at least once: Ripple Studio is entirely a passion project.
A fact that probably matters most to myself, but I assume is not entirely meaningless to anyone considering entrusting me with some kind of work.

Since art is inherently subjective in so many ways; even the concept itself is quite vaguely defined, it should be expected that it will be felt differently, expressed differently, and mean something different to everyone. When I was younger, I wanted to make music when I grew up, probably as a musician of some kind. 
That was before I had any concept of what "doing shit" when you grow up meant. The dream was simple and pure, only about making music, and a complete disregard for the implications of what "work" actually means. Doing what it takes to be able to afford bread.

And here's something I learned not long after: work fucking sucks. While I have no doubt that the more you enjoy what you do, the less suck work feels. But the reality of having to pay for bread can't be solved. While I genuinely admire all the artists with more courage than me who went down that path, 
I just couldn't find a way to make reality reconcile with my version of the dream. Music in such a commercial context strays completely opposite and is incompatible with what art means to me.

To finally get to the point, this means that while Ripple Studio is by all standards a business like any other commercial studio, the intention behind it is much simpler. Make some good fucking music, and have a good time doing it. Not relying on it to pay for bread. I'm not sure what impact, if any, my artistic intention has on my mixes, 
but I do hope some of it comes through in the music. It's one of the elements that I appreciate the most in not only music, but arts in general.

Now, to be clear, I am obviously not trying to say that I will impose my vision on a track, artist's wish be damned. It also doesn't mean that I will refuse to produce mixes that have all the elements to compete on modern streaming platforms. And it certainly doesn't mean that anyone has to share my views for a successful collaboration. 

This page really mostly serves as a reminder to myself, to ensure that the intention behind this project never changes.
