---
title: "About"
keywords:
  - story
  - studio
  - recording
  - music
maintitle : "Ripple Studio"
maindesc:
  
    "Ripple Studio was born out of a love for music and an obsession with high quality audio. It is, above all, a passion project. As such, I can be unapologetically opinionated in how the studio operates, as all artistic undertakings should be. My only concern is helping artists get the most out of their vision, and in the end, there is only one thing that really matters: making some good music!"
  
secondarytitle: "The very uninteresting life story"
secondarydesc:
  [
    "Classically trained as a violinist between from the age of three, trading sweet childhood memories for fluency in music theory, working on ear training instead of social skills. But at least I can brag about having perfect pitch, which might potentially be the worst party trick ever.", "Spent all my free time in my teens rebelling against classical music, exchanging Haydn for Hendrix, going from studying the seven modes to practising seven different ways to play the pentatonic, thinking I was Frusciante while the tape was rolling, only to hear more Nick Jonas during playback.", "Cut my losses early, as going to university killed the rock n' roll dream for good, and instead set my sights on a peaceful lifetime of familial comfort.", "Overcompensated by accumulating a pile of audio gear stacked so high that I could reach the top of the rock n' roll heap if I climbed it, thinking: as long as I can go home and spin the original pressing of the White Album on a Pro-Ject Signature 12 through a pair of EgglestonWorks Signature, live fast die young be damned.", "Staunch defender and believer in the magic of analog gear, despite any and all proof to the contrary. I refuse to believe this 50$ plugin sounds the same as my Silver Jubilee. Analog has so much more soul, I will be saying on my deathbed, still refusing any blind tests to backup my claims."
  ]
othertitle: ""
otherdesc:
  [
    ""
  ]
---
